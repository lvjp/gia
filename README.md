# Gitignore Aggregator

Just aggregate several `.gitignore` file from
[github/gitignore](https://github.com/github/gitignore) into one.

This is useful when you create a project with a brand new `.gitignore` file.

## Install

```bash
go get -u gitlab.com/lvjp/gia.git/go/cmd/gia
```

## License

Full license can be found in the [COPYING](COPYING) file.

    This file is part of Gitignore Aggregator.

    Gitignore Aggregator is free software: you can redistribute it
    and/or modify it under the terms of the GNU General Public License as
    published by the the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    Gitignore Aggregator is distributed in the hope that it will be
    useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Gitignore Aggregator.
    If not, see <https://www.gnu.org/licenses/>.
