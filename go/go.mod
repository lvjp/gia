module gitlab.com/lvjp/gia

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/google/go-github/v25 v25.0.2
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.2
)
