// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

var licenseCmd = &cobra.Command{
	Use:   "license",
	Short: "Show the license",
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(`This file is part of GitIgnore Aggregator.

GitIgnore Aggregator is free software: you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

GitIgnore Aggregator is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GitIgnore Aggregator.
If not, see <https://www.gnu.org/licenses/>.`)
	},
}

func init() {
	rootCmd.AddCommand(licenseCmd)
}
