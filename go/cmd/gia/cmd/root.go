// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var rootCmd = &cobra.Command{
	Use:   "gia",
	Short: "A simple GitIgnore file Aggregator",
	Long: `Creation or update of .gitignore file is made more easy

.gitignore templates files can now be aggregate with one command.
Templates files are fetched, by default, from https://github.com/github/gitignore.
Full documentation at: <https://gitlab.com/lvjp/gia>`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize()
}

func declareProviderFlags(cmd *cobra.Command) {
	cmd.Flags().String("provider", "github.api", "Select the .gitignore template provider")
	err := viper.BindPFlag("provider", cmd.Flags().Lookup("provider"))
	if err != nil {
		panic(err.Error())
	}
}
