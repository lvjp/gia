package main

import (
	"os"
	"testing"
)

// TestCobraViperStartup is just checking if all cobra and viper initialization neither error nor panic.
func TestCobraViperStartup(_ *testing.T) {
	os.Args = []string{
		os.Args[0],
		"--help",
	}

	main()
}
