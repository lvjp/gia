// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package utils

import (
	"testing"
)

func TestRegistryCreation(t *testing.T) {
	r := NewRegistry()

	if r.Len() != 0 {
		t.Fatal("Newly created registry should be empty")
	}
}

func TestEmptyKeyRegistration(t *testing.T) {
	r := NewRegistry()

	err := r.Register("", 3)
	if err == nil {
		t.FailNow()
	}
}

func TestEmptyValueRegistration(t *testing.T) {
	r := NewRegistry()

	err := r.Register("foobar", nil)
	if err == nil {
		t.FailNow()
	}
}

func TestRegistration(t *testing.T) {
	r := NewRegistry()

	if err := r.Register("foobar", "The foobar value"); err != nil {
		t.Fatal("Registration error of 'foobar':", err)
	}

	if len := r.Len(); len != 1 {
		t.Fatal("Len is expected to be 1, not:", len)
	}

	if value := r.Get("foobar"); value == nil {
		t.Fatal("Cannot fetch registered value")
	}

	if err := r.Register("foobar", "The another foobar value"); err == nil {
		t.Fatal("Double registration is ok !")
	}

	if len := r.Len(); len != 1 {
		t.Fatal("Len is expected to steal be 1 if registration failed, not:", len)
	}

	if err := r.Register("barfoo", "The barfoo value"); err != nil {
		t.Fatal("Registration error of 'barfoo'", err)
	}

	if len := r.Len(); len != 2 {
		t.Fatal("Len should be '2', not:", len)
	}

	r.Unregister("barfoo")

	if len := r.Len(); len != 1 {
		t.Fatal("Len should be returned to '1', not:", len)
	}

	if value := r.Get("barfoo"); value != nil {
		t.Fatal("Unregistered 'barfoo' should unregistered !")
	}

	r.Clear()

	if len := r.Len(); len != 0 {
		t.Fatal("After a clear, len should be '0', not:", len)
	}
}
