// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package utils

import (
	"fmt"
)

// Registry can do mapping between a string and an interface{}.
// Each entry cannot be override nor set to nil.
type Registry struct {
	data map[string]interface{}
}

// NewRegistry initialize an new empty registry.
func NewRegistry() (r Registry) {
	r = Registry{}
	r.Clear()
	return
}

// Clear the registry from all entries.
func (r *Registry) Clear() {
	r.data = make(map[string]interface{})
}

// Len return the number of registered entries.
func (r *Registry) Len() int {
	return len(r.data)
}

// Register add a new entry in the register.
// If the key is empty, value is nil or key is already registered,
// an error will be returned. On error, the registry is not altered.
func (r *Registry) Register(key string, value interface{}) (err error) {
	if len(key) == 0 {
		return fmt.Errorf("cannot register with an empty key")
	}

	if value == nil {
		return fmt.Errorf("cannot register '%s' with a nil value", key)
	}

	if _, present := r.data[key]; present {
		return fmt.Errorf("key '%s': already registered", key)
	}

	r.data[key] = value

	return nil
}

// Unregister delete a key from the registry.
func (r *Registry) Unregister(key string) {
	delete(r.data, key)
}

// Get return the value associated with the given key.
// If key is not registered, this function return nil.
func (r *Registry) Get(key string) interface{} {
	return r.data[key]
}
