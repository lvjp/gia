// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package providers

import (
	"context"
	"fmt"
	"reflect"

	"gitlab.com/lvjp/gia/internal/pkg/utils"
)

type Options map[string]interface{}
type Factory func(ctx context.Context, options Options) (EndPoint, error)

var factories = utils.NewRegistry()

// Register an association of a factory provider to his name.
// If the name is empty, already registered or when factory is nil, a panic is raised.
func Register(name string, factory Factory) {
	err := factories.Register(name, factory)
	if err != nil {
		panic(fmt.Sprintf("Cannot register '%s': %s", name, err.Error()))
	}
}

func Create(ctx context.Context, name string, options Options) (provider EndPoint, err error) {
	f, ok := factories.Get(name).(Factory)
	if f == nil {
		err = fmt.Errorf("provider '%s' do not exists", name)
		return
	}

	if !ok {
		panic(fmt.Sprintf("Provider '%s' have a bad type: %v", name, reflect.TypeOf(f)))
	}

	return f(ctx, options)
}

type InvalidProviderError struct {
	Name string
}

func (err InvalidProviderError) Error() string {
	return fmt.Sprintf("Provider not registered: '%s'", err.Name)
}
