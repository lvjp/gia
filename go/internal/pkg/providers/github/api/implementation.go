// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package api

import (
	"context"
	"errors"

	"github.com/google/go-github/v25/github"
	"gitlab.com/lvjp/gia/internal/pkg/providers"
)

func init() {
	providers.Register("github.api", instantiate)
}

type gitHubProvider struct {
	client  *github.Client
	context context.Context
}

func instantiate(ctx context.Context, _ providers.Options) (provider providers.EndPoint, err error) {
	if ctx == nil {
		err = errors.New("must not provide a nil context")
		return
	}

	provider = &gitHubProvider{
		client:  github.NewClient(nil),
		context: ctx,
	}
	return
}

func (p *gitHubProvider) Name() (name string) {
	return "GitHub Ignore API"
}

func (p *gitHubProvider) List() (templates []string, err error) {
	templates, _, err = p.client.Gitignores.List(p.context)
	if err != nil {
		return nil, err
	}

	return
}

func (p *gitHubProvider) Get(templateName string) (source string, err error) {
	gi, _, err := p.client.Gitignores.Get(p.context, templateName)
	if err != nil {
		return "", err
	}

	source = gi.GetSource()
	return
}
