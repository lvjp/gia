// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package fetch

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/lvjp/gia/internal/pkg/providers"
)

func Execute(args []string) {
	providerName := viper.GetString("provider")

	provider, err := providers.Create(context.Background(), providerName, nil)
	if err != nil {
		log.Fatalf("Cannot load provider '%s': %s", providerName, err.Error())
	}

	var haveError = false

	for _, templateName := range args {
		source, err := provider.Get(templateName)
		if err != nil {
			haveError = true
			_, _ = fmt.Fprintf(
				os.Stderr,
				"Cannot fetch template '%s', cause: %v\n",
				templateName,
				err,
			)
			continue
		}

		marker := fmt.Sprintf("# -*- %s '%s' -*- #", provider.Name(), templateName)
		fmt.Println(marker)

		fmt.Print(source)
		if len(source) == 0 || source[len(source)-1:] != "\n" {
			fmt.Println()
		}

		fmt.Println(marker)
	}

	if haveError {
		os.Exit(1)
	}
}
