// This file is part of GitIgnore Aggregator.
//
// GitIgnore Aggregator is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as
// published by the the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// GitIgnore Aggregator is distributed in the hope that it will be
// useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with GitIgnore Aggregator.
// If not, see <https://www.gnu.org/licenses/>.

package list

import (
	"context"
	"fmt"
	"log"
	"sort"

	"github.com/spf13/viper"
	"gitlab.com/lvjp/gia/internal/pkg/providers"
)

func Execute() {
	providerName := viper.GetString("provider")

	provider, err := providers.Create(context.Background(), providerName, nil)
	if err != nil {
		log.Fatalf("Cannot load provider '%s': %s", providerName, err.Error())
	}

	templates, err := provider.List()
	if err != nil {
		log.Fatal("Cannot fetch template list:", err.Error())
	}

	sort.Strings(templates)

	for _, template := range templates {
		fmt.Println(template)
	}
}
