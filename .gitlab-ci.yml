# vim:set ts=2 sw=2 tw=95 et:

stages:
  - check
  - build
  - packaging
  - deploy

.go:
  image: golang:1.12.4
  before_script:
    - cd go
    - go version
    - go env GOPATH
    - go mod download
    - go mod verify
  cache:
    paths:
      - .go
  variables:
    GOPATH: "${CI_PROJECT_DIR}/.go"

go:mod:tidy:
  stage: check
  extends: .go
  script:
    - go mod tidy
    - cd "$(git rev-parse --show-toplevel)"
    - . "$(git --exec-path)/git-sh-setup"
    - require_clean_work_tree "validate go.mod file" "Please check 'go mod tidy' result"

go:vet:
  stage: check
  extends: .go
  script:
    - go vet ./...

go:fmt:
  stage: check
  extends: .go
  script:
    - gofmt -d -s .
    - test -z $(gofmt -l -s .)

go:golangci-lint:
  stage: check
  extends: .go
  image: golangci/golangci-lint:v1.16.0
  script:
    - golangci-lint --version
    - golangci-lint run

go:lint:
  stage: check
  extends: .go
  script:
    - export "PATH=${GOPATH}/bin:${PATH}"
    - go get golang.org/x/lint/golint@959b441ac422379a43da2230f62be024250818b0
    - golint -set_exit_status ./...
  allow_failure: true

go:coverage:
  extends: .go
  stage: build
  script:
    - mkdir dist
    - go test -coverpkg=./... -coverprofile=dist/coverage.out ./...
    - go tool cover -func=dist/coverage.out
    - go tool cover -html=dist/coverage.out -o dist/coverage.html
  artifacts:
    paths:
      - go/dist
  coverage: '/total:\t+\(statements\)\t+(\d+\.\d+)%/'

.go:build:
  extends: .go
  stage: build
  script:
    - mkdir dist
    - go build -o "dist/gia.${GOOS}-${GOARCH}${GOEXE}" ./cmd/gia
  artifacts:
    paths:
      - go/dist

go:build:darwin:386:
  extends: .go:build
  variables:
    GOOS: darwin
    GOARCH: 386

go:build:darwin:amd64:
  extends: .go:build
  variables:
    GOOS: darwin
    GOARCH: amd64

go:build:freebsd:386:
  extends: .go:build
  variables:
    GOOS: freebsd
    GOARCH: 386

go:build:freebsd:amd64:
  extends: .go:build
  variables:
    GOOS: freebsd
    GOARCH: amd64

go:build:linux:386:
  extends: .go:build
  variables:
    GOOS: linux
    GOARCH: 386

go:build:linux:amd64:
  extends: .go:build
  variables:
    GOOS: linux
    GOARCH: amd64

go:build:linux:arm:
  extends: .go:build
  variables:
    GOOS: linux
    GOARCH: arm

go:build:linux:arm64:
  extends: .go:build
  variables:
    GOOS: linux
    GOARCH: arm64

go:build:linux:ppc64le:
  extends: .go:build
  variables:
    GOOS: linux
    GOARCH: ppc64le

go:build:windows:386:
  extends: .go:build
  variables:
    GOOS: windows
    GOARCH: 386
    GOEXE: .exe

go:build:windows:amd64:
  extends: .go:build
  variables:
    GOOS: windows
    GOARCH: amd64
    GOEXE: .exe

pages:artifacts:
  image: busybox:1.30.1
  stage: packaging
  script:
    - find go -type f
    - mkdir public
    - mv pages/* public/
    - mv go/dist public/go
  artifacts:
    paths:
      - public

pages:
  image: busybox:1.30.1
  stage: deploy
  script:
    - /bin/true
  dependencies:
    - pages:artifacts
  artifacts:
    paths:
      - public
  only:
    - develop
